package com.universal.converter.service;

public interface ConvertService {

  public double convert(String from, String to);

  public int correct(String from, String to);

  public boolean union(String x, String y);

  public void consctruct();
}
