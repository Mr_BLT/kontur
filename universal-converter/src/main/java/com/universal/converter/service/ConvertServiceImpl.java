package com.universal.converter.service;

import com.universal.converter.repository.ConvertRepository;
import com.universal.converter.util.Arc;
import com.universal.converter.util.Edge;
import com.universal.converter.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ConvertServiceImpl implements ConvertService {

  private HashMap<String, Integer> hashVerticex;
  private ArrayList<ArrayList<Arc>> graph;

  private int[] component;
  private double[] weighthVerticex;

  private int n = 0;

  @Autowired
  public ConvertRepository convertRepository;

  @PostConstruct
  public void consctruct() {
    hashVerticex = new HashMap<>();
    graph = new ArrayList<>();
    List<Edge> edges = convertRepository.getListEdge();

    for (int i = 0; i < edges.size(); i++) {
      Edge edge = edges.get(i);

      String x = edge.getX();
      String y = edge.getY();
      double weight = edge.getWeight();

      if (!hashVerticex.containsKey(x)) {
        hashVerticex.put(x, n);
        graph.add(new ArrayList<>());
        n++;
      }

      if (!hashVerticex.containsKey(y)) {
        hashVerticex.put(y, n);
        graph.add(new ArrayList<>());
        n++;
      }

      int v = hashVerticex.get(x);
      int w = hashVerticex.get(y);

      graph.get(v).add(Arc.builder().point(w).weight(weight).fraction(false).build());
      graph.get(w).add(Arc.builder().point(v).weight(weight).fraction(true).build());
    }

    graphTraversal(n);
  }

  @Override
  public double convert(String from, String to) {
    String[] arrayFrom = from.split(" ");
    String[] arrayTo = to.split(" ");

    List<String> listFrom = new ArrayList<>();
    List<String> listTo = new ArrayList<>();

    List<String> listFrom2 = new ArrayList<>();
    List<String> listTo2 = new ArrayList<>();

    boolean typeFrom = true, typeTo = true;

    for (int i = 0; i < arrayFrom.length; i++) {
      if (arrayFrom[i].equals("/")) {
        typeFrom = false;
        continue;
      }
      if (typeFrom) {
        if (!arrayFrom[i].equals("*")) {
          listFrom.add(arrayFrom[i]);
        }
      } else {
        if (!arrayFrom[i].equals("*")) {
          listFrom2.add(arrayFrom[i]);
        }
      }
    }

    for (int i = 0; i < arrayTo.length; i++) {
      if (arrayTo[i].equals("/")) {
        typeTo = false;
        continue;
      }
      if (typeTo) {
        if (!arrayTo[i].equals("*")) {
          listTo.add(arrayTo[i]);
        }
      } else {
        if (!arrayTo[i].equals("*")) {
          listTo2.add(arrayTo[i]);
        }
      }
    }

    // одникавое ли выражение.Если есть у одного "/" то и друго должно быть
    if (typeFrom != typeTo) {
      return 0;
    }
    if (typeFrom) {
      double numerator = convertString(listFrom, listTo);
      return numerator;
    } else {
      double numerator = convertString(listFrom, listTo);
      double denominator = convertString(listFrom2, listTo2);
      double ans = numerator / denominator;
      return ans;
    }
  }

  // Если выдаст 0 значит есть элемент без пары.Который нельзя перевести в другую систему.
  private double convertString(List<String> listFrom, List<String> listTo) {

    double ans = 1.0;
    boolean[] used = new boolean[listTo.size()];

    for (int i = 0; i < listFrom.size(); i++) {
      int v = hashVerticex.get(listFrom.get(i));
      int groupV = component[v];
      boolean usedV = false;

      for (int j = 0; j < listTo.size(); j++) {
        int w = hashVerticex.get(listTo.get(j));
        int groupW = component[w];

        if (groupV != groupW) {
          continue;
        }

        used[j] = true;
        usedV = true;

        double weighthV = weighthVerticex[v];
        double weighthW = weighthVerticex[w];

        // w/v узнаем их разность то есть v -> w чему равно

        double a = weighthW / weighthV;
        ans = ans * a;
      }

      // использовали ли мы этот элемент из listFrom?
      if (!usedV) {
        return 0.0;
      }
    }

    // все ли мы использовали элименты из  listTo?
    for (int i = 0; i < listTo.size(); i++) {
      if (!used[i]) {
        return 0.0;
      }
    }

    return ans;
  }

  // проверяем все ли элементы допустимы
  @Override
  public int correct(String from, String to) {
    boolean typeFrom = Arrays.stream(from.split(" "))
        .allMatch(word -> hashVerticex.containsKey(word) || word.equals("/") || word.equals("*"));
    boolean typeTo = Arrays.stream(to.split(" "))
        .allMatch(word -> hashVerticex.containsKey(word) || word.equals("/") || word.equals("*"));
    if (typeFrom && typeTo) {
      return 200;
    }
    return 400;
  }

  @Override
  public boolean union(String x, String y) {
    return false;
  }

  //создаем граф и запускаем метод в каждой компоненты bfs()
  private void graphTraversal(int n) {
    component = new int[n];
    weighthVerticex = new double[n];
    List<Pair<Integer>> pairs = new ArrayList<>();

    for (int i = 0; i < n; i++) {
      pairs.add(new Pair<>());
      pairs.get(i).setKey(i);
      pairs.get(i).setValue(graph.get(i).size());
    }

    pairs = pairs.stream().sorted(new Comparator<Pair<Integer>>() {
      @Override
      public int compare(Pair<Integer> o1, Pair<Integer> o2) {
        if (o1.getValue() > o2.getValue()) {
          return -1;
        }
        return 1;
      }
    }).collect(Collectors.toList());

    int index = 1;
    for (int i = 0; i < n; i++) {
      int x = pairs.get(i).getKey();
      if (component[x] == 0) {
        bfs(x, index);
        index++;
      }
    }
  }

  // обходим компнонет графа и сразу даем вес относительно вершины,у которой больше всего ребер.Это первая вершина которую обходим
  private void bfs(int x, int index) {
    Queue<Integer> queue = new LinkedList<>();
    queue.add(x);

    weighthVerticex[x] = 1.0;
    component[x] = index;

    while (!queue.isEmpty()) {
      int v = queue.poll();
      double weighthV = weighthVerticex[v];

      for (Arc arc : graph.get(v)) {
        int y = arc.getPoint();
        if (component[y] == 0) {
          double weighthW = arc.getWeight();
          component[y] = index;
          // isFaction() = true значит weighthW = (1.0)/(число)
          if (arc.isFraction()) {
            weighthVerticex[y] = 1 / (weighthV * weighthW);
          } else {
            weighthVerticex[y] = weighthW / weighthV;
          }

        }
      }
    }
  }

}
