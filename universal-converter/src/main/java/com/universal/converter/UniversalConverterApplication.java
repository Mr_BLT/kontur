package com.universal.converter;

import java.util.Collections;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversalConverterApplication {

  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(UniversalConverterApplication.class);
    app.setDefaultProperties(Collections.singletonMap("path.file", args[0]));
    app.run(args);
  }
}
