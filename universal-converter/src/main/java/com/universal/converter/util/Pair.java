package com.universal.converter.util;

import lombok.Data;

@Data
public class Pair<T> {

  private T key;
  private T value;
}
