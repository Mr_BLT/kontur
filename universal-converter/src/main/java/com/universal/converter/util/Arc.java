package com.universal.converter.util;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class Arc {

  private int point;
  private double weight;
  private boolean fraction;// делим мы weight на 1?
}
