package com.universal.converter.controller;


import com.universal.converter.model.UserDto;
import com.universal.converter.service.ConvertService;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UniversalConverterController {

  @Autowired
  public ConvertService convertService;


  @PostMapping("/convert")
  public ResponseEntity<?> getConvert(@RequestBody UserDto userDto) {
    String from = userDto.getFrom();
    String to = userDto.getTo();

    int status = convertService.correct(from, to);
    ResponseEntity responseEntity;

    if (status == 400) {
      responseEntity = ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    } else {
      double ans = convertService.convert(from, to);
      if (ans == 0) {
        responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
      } else {
        NumberFormat nf = new DecimalFormat("##.###############");// для формата вывода
        responseEntity = ResponseEntity.status(HttpStatus.OK)
            .body(nf.format(ans));
      }
    }
    return responseEntity;
  }
}
