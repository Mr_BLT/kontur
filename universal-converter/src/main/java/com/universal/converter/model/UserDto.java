package com.universal.converter.model;

import lombok.Data;

@Data
public class UserDto {

  private String from;
  private String to;
}
