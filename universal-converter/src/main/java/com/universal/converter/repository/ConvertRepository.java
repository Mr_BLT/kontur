package com.universal.converter.repository;

import com.universal.converter.util.Edge;

import java.util.List;

public interface ConvertRepository {

  public List<Edge> getListEdge();
}
