package com.universal.converter.repository;

import com.universal.converter.util.Edge;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ConvertRepositoryImpl implements ConvertRepository {

  private BufferedReader bufferedReader;

  private List<Edge> edgeList;

  @Value("${path.file}")
  private String path;

  @PostConstruct
  public void init() {
    edgeList = new ArrayList<>();
    try {
      bufferedReader = new BufferedReader(new FileReader(new File(path)));

      while (bufferedReader.ready()) {
        String string = bufferedReader.readLine();
        String[] strings = string.split(",");

        Edge edge = Edge.builder()
            .x(strings[0])
            .y(strings[1])
            .weight(Double.parseDouble(strings[2]))
            .build();

        edgeList.add(edge);
      }
      bufferedReader.close();
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @Override
  public List<Edge> getListEdge() {
    return edgeList;
  }
}
